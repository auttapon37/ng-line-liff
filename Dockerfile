FROM nginx:alpine
COPY ./dist /usr/share/nginx/html
COPY ./config/nginx-custom.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]



